filename=digitaltypography

default: pdf
all: clean pdf

pdf: ${filename}.pdf

%.pdf: ${filename}.tex
	xelatex -interaction=nonstopmode $<
	makeindex $*
	bibtex $*
	xelatex -interaction=nonstopmode $< # to include generated ToC
	xelatex -interaction=nonstopmode $< # to include bibliography

clean:
	rm -f ${filename}.pdf ${filename}.log ${filename}.aux ${filename}.out ${filename}.bbl ${filename}.blg ${filename}.ind ${filename}*.xml
	rm -f ${filename}.tmp ${filename}.xdv ${filename}.xref ${filename}*.html ${filename}.lg ${filename}.css ${filename}.4* ${filename}.id*

html: ${filename}.tex
	htxelatex $< "articlehtml,html,word, charset=utf-8"